## 5.17.0

- Add new key constants
    - `KEY_ALL_APPLICATIONS`
    - `KEY_DICTATE`

## 5.16.8

- Change the version number to mach the kernel version
- Replaced constants with macros making values not typed, this eliminates the need for casting since different APIs use
  different integer sizes

## 0.2.2

- Allow building on android
- Add new key constants
    - `KEY_NOTIFICATION_CENTER`
    - `KEY_PICKUP_PHONE`
    - `KEY_HANGUP_PHONE`
    - `KEY_FN_RIGHT_SHIFT`
    - `KEY_KBD_LAYOUT_NEXT`
    - `KEY_EMOJI_PICKER`
    - `KEY_PRIVACY_SCREEN_TOGGLE`
    - `KEY_SELECTIVE_SCREENSHOT`
    - `KEY_MACRO1`
    - `KEY_MACRO30`
    - `KEY_MACRO_RECORD_START`
    - `KEY_MACRO_RECORD_STOP`
    - `KEY_MACRO_PRESET_CYCLE`
    - `KEY_MACRO_PRESET1`
    - `KEY_MACRO_PRESET3`
    - `KEY_KBD_LCD_MENU1`
    - `KEY_KBD_LCD_MENU5`
- Add new sw constants
    - `SW_MACHINE_COVER`
- Updated `SW_MAX` value

## 0.2.1

- Make it so the project only builds on Linux

## 0.2.0

- Change type from `u8` to `u16` for evdev compatibility

## 0.1.1

- Update Cargo.toml

## 0.1.0

- Initial release
