#![allow(non_snake_case)]

use input_event_codes::{SYN_CNT, SYN_CONFIG, SYN_DROPPED, SYN_MAX, SYN_MT_REPORT, SYN_REPORT};

#[test]
fn test_SYN_REPORT() {
	assert_eq!(SYN_REPORT!(), 0);
}

#[test]
fn test_SYN_CONFIG() {
	assert_eq!(SYN_CONFIG!(), 1);
}

#[test]
fn test_SYN_MT_REPORT() {
	assert_eq!(SYN_MT_REPORT!(), 2);
}

#[test]
fn test_SYN_DROPPED() {
	assert_eq!(SYN_DROPPED!(), 3);
}

#[test]
fn test_SYN_MAX() {
	assert_eq!(SYN_MAX!(), 0xf);
}

#[test]
fn test_SYN_CNT() {
	assert_eq!(SYN_CNT!(), (SYN_MAX!() + 1));
}
