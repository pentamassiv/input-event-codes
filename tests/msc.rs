#![allow(non_snake_case)]

use input_event_codes::{MSC_CNT, MSC_GESTURE, MSC_MAX, MSC_PULSELED, MSC_RAW, MSC_SCAN, MSC_SERIAL, MSC_TIMESTAMP};

#[test]
fn test_MSC_SERIAL() {
	assert_eq!(MSC_SERIAL!(), 0x00);
}

#[test]
fn test_MSC_PULSELED() {
	assert_eq!(MSC_PULSELED!(), 0x01);
}

#[test]
fn test_MSC_GESTURE() {
	assert_eq!(MSC_GESTURE!(), 0x02);
}

#[test]
fn test_MSC_RAW() {
	assert_eq!(MSC_RAW!(), 0x03);
}

#[test]
fn test_MSC_SCAN() {
	assert_eq!(MSC_SCAN!(), 0x04);
}

#[test]
fn test_MSC_TIMESTAMP() {
	assert_eq!(MSC_TIMESTAMP!(), 0x05);
}

#[test]
fn test_MSC_MAX() {
	assert_eq!(MSC_MAX!(), 0x07);
}

#[test]
fn test_MSC_CNT() {
	assert_eq!(MSC_CNT!(), (MSC_MAX!() + 1));
}
