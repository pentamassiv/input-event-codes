#![allow(non_snake_case)]

use input_event_codes::{
	ABS_BRAKE, ABS_CNT, ABS_DISTANCE, ABS_GAS, ABS_HAT0X, ABS_HAT0Y, ABS_HAT1X, ABS_HAT1Y, ABS_HAT2X, ABS_HAT2Y,
	ABS_HAT3X, ABS_HAT3Y, ABS_MAX, ABS_MISC, ABS_MT_BLOB_ID, ABS_MT_DISTANCE, ABS_MT_ORIENTATION, ABS_MT_POSITION_X,
	ABS_MT_POSITION_Y, ABS_MT_PRESSURE, ABS_MT_SLOT, ABS_MT_TOOL_TYPE, ABS_MT_TOOL_X, ABS_MT_TOOL_Y,
	ABS_MT_TOUCH_MAJOR, ABS_MT_TOUCH_MINOR, ABS_MT_TRACKING_ID, ABS_MT_WIDTH_MAJOR, ABS_MT_WIDTH_MINOR, ABS_PRESSURE,
	ABS_RESERVED, ABS_RUDDER, ABS_RX, ABS_RY, ABS_RZ, ABS_THROTTLE, ABS_TILT_X, ABS_TILT_Y, ABS_TOOL_WIDTH, ABS_VOLUME,
	ABS_WHEEL, ABS_X, ABS_Y, ABS_Z,
};

#[test]
fn test_ABS_X() {
	assert_eq!(ABS_X!(), 0x00);
}

#[test]
fn test_ABS_Y() {
	assert_eq!(ABS_Y!(), 0x01);
}

#[test]
fn test_ABS_Z() {
	assert_eq!(ABS_Z!(), 0x02);
}

#[test]
fn test_ABS_RX() {
	assert_eq!(ABS_RX!(), 0x03);
}

#[test]
fn test_ABS_RY() {
	assert_eq!(ABS_RY!(), 0x04);
}

#[test]
fn test_ABS_RZ() {
	assert_eq!(ABS_RZ!(), 0x05);
}

#[test]
fn test_ABS_THROTTLE() {
	assert_eq!(ABS_THROTTLE!(), 0x06);
}

#[test]
fn test_ABS_RUDDER() {
	assert_eq!(ABS_RUDDER!(), 0x07);
}

#[test]
fn test_ABS_WHEEL() {
	assert_eq!(ABS_WHEEL!(), 0x08);
}

#[test]
fn test_ABS_GAS() {
	assert_eq!(ABS_GAS!(), 0x09);
}

#[test]
fn test_ABS_BRAKE() {
	assert_eq!(ABS_BRAKE!(), 0x0a);
}

#[test]
fn test_ABS_HAT0X() {
	assert_eq!(ABS_HAT0X!(), 0x10);
}

#[test]
fn test_ABS_HAT0Y() {
	assert_eq!(ABS_HAT0Y!(), 0x11);
}

#[test]
fn test_ABS_HAT1X() {
	assert_eq!(ABS_HAT1X!(), 0x12);
}

#[test]
fn test_ABS_HAT1Y() {
	assert_eq!(ABS_HAT1Y!(), 0x13);
}

#[test]
fn test_ABS_HAT2X() {
	assert_eq!(ABS_HAT2X!(), 0x14);
}

#[test]
fn test_ABS_HAT2Y() {
	assert_eq!(ABS_HAT2Y!(), 0x15);
}

#[test]
fn test_ABS_HAT3X() {
	assert_eq!(ABS_HAT3X!(), 0x16);
}

#[test]
fn test_ABS_HAT3Y() {
	assert_eq!(ABS_HAT3Y!(), 0x17);
}

#[test]
fn test_ABS_PRESSURE() {
	assert_eq!(ABS_PRESSURE!(), 0x18);
}

#[test]
fn test_ABS_DISTANCE() {
	assert_eq!(ABS_DISTANCE!(), 0x19);
}

#[test]
fn test_ABS_TILT_X() {
	assert_eq!(ABS_TILT_X!(), 0x1a);
}

#[test]
fn test_ABS_TILT_Y() {
	assert_eq!(ABS_TILT_Y!(), 0x1b);
}

#[test]
fn test_ABS_TOOL_WIDTH() {
	assert_eq!(ABS_TOOL_WIDTH!(), 0x1c);
}

#[test]
fn test_ABS_VOLUME() {
	assert_eq!(ABS_VOLUME!(), 0x20);
}

#[test]
fn test_ABS_MISC() {
	assert_eq!(ABS_MISC!(), 0x28);
}

#[test]
fn test_ABS_RESERVED() {
	assert_eq!(ABS_RESERVED!(), 0x2e);
}

#[test]
fn test_ABS_MT_SLOT() {
	assert_eq!(ABS_MT_SLOT!(), 0x2f);
}

#[test]
fn test_ABS_MT_TOUCH_MAJOR() {
	assert_eq!(ABS_MT_TOUCH_MAJOR!(), 0x30);
}

#[test]
fn test_ABS_MT_TOUCH_MINOR() {
	assert_eq!(ABS_MT_TOUCH_MINOR!(), 0x31);
}

#[test]
fn test_ABS_MT_WIDTH_MAJOR() {
	assert_eq!(ABS_MT_WIDTH_MAJOR!(), 0x32);
}

#[test]
fn test_ABS_MT_WIDTH_MINOR() {
	assert_eq!(ABS_MT_WIDTH_MINOR!(), 0x33);
}

#[test]
fn test_ABS_MT_ORIENTATION() {
	assert_eq!(ABS_MT_ORIENTATION!(), 0x34);
}

#[test]
fn test_ABS_MT_POSITION_X() {
	assert_eq!(ABS_MT_POSITION_X!(), 0x35);
}

#[test]
fn test_ABS_MT_POSITION_Y() {
	assert_eq!(ABS_MT_POSITION_Y!(), 0x36);
}

#[test]
fn test_ABS_MT_TOOL_TYPE() {
	assert_eq!(ABS_MT_TOOL_TYPE!(), 0x37);
}

#[test]
fn test_ABS_MT_BLOB_ID() {
	assert_eq!(ABS_MT_BLOB_ID!(), 0x38);
}

#[test]
fn test_ABS_MT_TRACKING_ID() {
	assert_eq!(ABS_MT_TRACKING_ID!(), 0x39);
}

#[test]
fn test_ABS_MT_PRESSURE() {
	assert_eq!(ABS_MT_PRESSURE!(), 0x3a);
}

#[test]
fn test_ABS_MT_DISTANCE() {
	assert_eq!(ABS_MT_DISTANCE!(), 0x3b);
}

#[test]
fn test_ABS_MT_TOOL_X() {
	assert_eq!(ABS_MT_TOOL_X!(), 0x3c);
}

#[test]
fn test_ABS_MT_TOOL_Y() {
	assert_eq!(ABS_MT_TOOL_Y!(), 0x3d);
}

#[test]
fn test_ABS_MAX() {
	assert_eq!(ABS_MAX!(), 0x3f);
}

#[test]
fn test_ABS_CNT() {
	assert_eq!(ABS_CNT!(), (ABS_MAX!() + 1));
}
