#![allow(non_snake_case)]

use input_event_codes::{
	EV_ABS, EV_CNT, EV_FF, EV_FF_STATUS, EV_KEY, EV_LED, EV_MAX, EV_MSC, EV_PWR, EV_REL, EV_REP, EV_SND, EV_SW, EV_SYN,
};

#[test]
fn test_EV_SYN() {
	assert_eq!(EV_SYN!(), 0x00);
}

#[test]
fn test_EV_KEY() {
	assert_eq!(EV_KEY!(), 0x01);
}

#[test]
fn test_EV_REL() {
	assert_eq!(EV_REL!(), 0x02);
}

#[test]
fn test_EV_ABS() {
	assert_eq!(EV_ABS!(), 0x03);
}

#[test]
fn test_EV_MSC() {
	assert_eq!(EV_MSC!(), 0x04);
}

#[test]
fn test_EV_SW() {
	assert_eq!(EV_SW!(), 0x05);
}

#[test]
fn test_EV_LED() {
	assert_eq!(EV_LED!(), 0x11);
}

#[test]
fn test_EV_SND() {
	assert_eq!(EV_SND!(), 0x12);
}

#[test]
fn test_EV_REP() {
	assert_eq!(EV_REP!(), 0x14);
}

#[test]
fn test_EV_FF() {
	assert_eq!(EV_FF!(), 0x15);
}

#[test]
fn test_EV_PWR() {
	assert_eq!(EV_PWR!(), 0x16);
}

#[test]
fn test_EV_FF_STATUS() {
	assert_eq!(EV_FF_STATUS!(), 0x17);
}

#[test]
fn test_EV_MAX() {
	assert_eq!(EV_MAX!(), 0x1f);
}

#[test]
fn test_EV_CNT() {
	assert_eq!(EV_CNT!(), (EV_MAX!() + 1));
}
