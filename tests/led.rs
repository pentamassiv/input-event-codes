#![allow(non_snake_case)]

use input_event_codes::{
	LED_CAPSL, LED_CHARGING, LED_CNT, LED_COMPOSE, LED_KANA, LED_MAIL, LED_MAX, LED_MISC, LED_MUTE, LED_NUML,
	LED_SCROLLL, LED_SLEEP, LED_SUSPEND,
};

#[test]
fn test_LED_NUML() {
	assert_eq!(LED_NUML!(), 0x00);
}

#[test]
fn test_LED_CAPSL() {
	assert_eq!(LED_CAPSL!(), 0x01);
}

#[test]
fn test_LED_SCROLLL() {
	assert_eq!(LED_SCROLLL!(), 0x02);
}

#[test]
fn test_LED_COMPOSE() {
	assert_eq!(LED_COMPOSE!(), 0x03);
}

#[test]
fn test_LED_KANA() {
	assert_eq!(LED_KANA!(), 0x04);
}

#[test]
fn test_LED_SLEEP() {
	assert_eq!(LED_SLEEP!(), 0x05);
}

#[test]
fn test_LED_SUSPEND() {
	assert_eq!(LED_SUSPEND!(), 0x06);
}

#[test]
fn test_LED_MUTE() {
	assert_eq!(LED_MUTE!(), 0x07);
}

#[test]
fn test_LED_MISC() {
	assert_eq!(LED_MISC!(), 0x08);
}

#[test]
fn test_LED_MAIL() {
	assert_eq!(LED_MAIL!(), 0x09);
}

#[test]
fn test_LED_CHARGING() {
	assert_eq!(LED_CHARGING!(), 0x0a);
}

#[test]
fn test_LED_MAX() {
	assert_eq!(LED_MAX!(), 0x0f);
}

#[test]
fn test_LED_CNT() {
	assert_eq!(LED_CNT!(), (LED_MAX!() + 1));
}
