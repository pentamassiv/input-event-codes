#![allow(non_snake_case)]

use input_event_codes::{REP_CNT, REP_DELAY, REP_MAX, REP_PERIOD};

#[test]
fn test_REP_DELAY() {
	assert_eq!(REP_DELAY!(), 0x00);
}

#[test]
fn test_REP_PERIOD() {
	assert_eq!(REP_PERIOD!(), 0x01);
}

#[test]
fn test_REP_MAX() {
	assert_eq!(REP_MAX!(), 0x01);
}

#[test]
fn test_REP_CNT() {
	assert_eq!(REP_CNT!(), (REP_MAX!() + 1));
}
