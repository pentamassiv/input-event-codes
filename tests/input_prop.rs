#![allow(non_snake_case)]

use input_event_codes::{
	INPUT_PROP_ACCELEROMETER, INPUT_PROP_BUTTONPAD, INPUT_PROP_CNT, INPUT_PROP_DIRECT, INPUT_PROP_MAX,
	INPUT_PROP_POINTER, INPUT_PROP_POINTING_STICK, INPUT_PROP_SEMI_MT, INPUT_PROP_TOPBUTTONPAD,
};

#[test]
fn test_INPUT_PROP_POINTER() {
	assert_eq!(INPUT_PROP_POINTER!(), 0x00);
}

#[test]
fn test_INPUT_PROP_DIRECT() {
	assert_eq!(INPUT_PROP_DIRECT!(), 0x01);
}

#[test]
fn test_INPUT_PROP_BUTTONPAD() {
	assert_eq!(INPUT_PROP_BUTTONPAD!(), 0x02);
}

#[test]
fn test_INPUT_PROP_SEMI_MT() {
	assert_eq!(INPUT_PROP_SEMI_MT!(), 0x03);
}

#[test]
fn test_INPUT_PROP_TOPBUTTONPAD() {
	assert_eq!(INPUT_PROP_TOPBUTTONPAD!(), 0x04);
}

#[test]
fn test_INPUT_PROP_POINTING_STICK() {
	assert_eq!(INPUT_PROP_POINTING_STICK!(), 0x05);
}

#[test]
fn test_INPUT_PROP_ACCELEROMETER() {
	assert_eq!(INPUT_PROP_ACCELEROMETER!(), 0x06);
}

#[test]
fn test_INPUT_PROP_MAX() {
	assert_eq!(INPUT_PROP_MAX!(), 0x1f);
}

#[test]
fn test_INPUT_PROP_CNT() {
	assert_eq!(INPUT_PROP_CNT!(), (INPUT_PROP_MAX!() + 1));
}
