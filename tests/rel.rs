#![allow(non_snake_case)]

use input_event_codes::{
	REL_CNT, REL_DIAL, REL_HWHEEL, REL_HWHEEL_HI_RES, REL_MAX, REL_MISC, REL_RESERVED, REL_RX, REL_RY, REL_RZ,
	REL_WHEEL, REL_WHEEL_HI_RES, REL_X, REL_Y, REL_Z,
};

#[test]
fn test_REL_X() {
	assert_eq!(REL_X!(), 0x00);
}

#[test]
fn test_REL_Y() {
	assert_eq!(REL_Y!(), 0x01);
}

#[test]
fn test_REL_Z() {
	assert_eq!(REL_Z!(), 0x02);
}

#[test]
fn test_REL_RX() {
	assert_eq!(REL_RX!(), 0x03);
}

#[test]
fn test_REL_RY() {
	assert_eq!(REL_RY!(), 0x04);
}

#[test]
fn test_REL_RZ() {
	assert_eq!(REL_RZ!(), 0x05);
}

#[test]
fn test_REL_HWHEEL() {
	assert_eq!(REL_HWHEEL!(), 0x06);
}

#[test]
fn test_REL_DIAL() {
	assert_eq!(REL_DIAL!(), 0x07);
}

#[test]
fn test_REL_WHEEL() {
	assert_eq!(REL_WHEEL!(), 0x08);
}

#[test]
fn test_REL_MISC() {
	assert_eq!(REL_MISC!(), 0x09);
}

#[test]
fn test_REL_RESERVED() {
	assert_eq!(REL_RESERVED!(), 0x0a);
}

#[test]
fn test_REL_WHEEL_HI_RES() {
	assert_eq!(REL_WHEEL_HI_RES!(), 0x0b);
}

#[test]
fn test_REL_HWHEEL_HI_RES() {
	assert_eq!(REL_HWHEEL_HI_RES!(), 0x0c);
}

#[test]
fn test_REL_MAX() {
	assert_eq!(REL_MAX!(), 0x0f);
}

#[test]
fn test_REL_CNT() {
	assert_eq!(REL_CNT!(), (REL_MAX!() + 1));
}
