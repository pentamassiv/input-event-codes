#[macro_export]
macro_rules! ABS_X {
	() => {
		0x00
	};
}
#[macro_export]
macro_rules! ABS_Y {
	() => {
		0x01
	};
}
#[macro_export]
macro_rules! ABS_Z {
	() => {
		0x02
	};
}
#[macro_export]
macro_rules! ABS_RX {
	() => {
		0x03
	};
}
#[macro_export]
macro_rules! ABS_RY {
	() => {
		0x04
	};
}
#[macro_export]
macro_rules! ABS_RZ {
	() => {
		0x05
	};
}
#[macro_export]
macro_rules! ABS_THROTTLE {
	() => {
		0x06
	};
}
#[macro_export]
macro_rules! ABS_RUDDER {
	() => {
		0x07
	};
}
#[macro_export]
macro_rules! ABS_WHEEL {
	() => {
		0x08
	};
}
#[macro_export]
macro_rules! ABS_GAS {
	() => {
		0x09
	};
}
#[macro_export]
macro_rules! ABS_BRAKE {
	() => {
		0x0a
	};
}
#[macro_export]
macro_rules! ABS_HAT0X {
	() => {
		0x10
	};
}
#[macro_export]
macro_rules! ABS_HAT0Y {
	() => {
		0x11
	};
}
#[macro_export]
macro_rules! ABS_HAT1X {
	() => {
		0x12
	};
}
#[macro_export]
macro_rules! ABS_HAT1Y {
	() => {
		0x13
	};
}
#[macro_export]
macro_rules! ABS_HAT2X {
	() => {
		0x14
	};
}
#[macro_export]
macro_rules! ABS_HAT2Y {
	() => {
		0x15
	};
}
#[macro_export]
macro_rules! ABS_HAT3X {
	() => {
		0x16
	};
}
#[macro_export]
macro_rules! ABS_HAT3Y {
	() => {
		0x17
	};
}
#[macro_export]
macro_rules! ABS_PRESSURE {
	() => {
		0x18
	};
}
#[macro_export]
macro_rules! ABS_DISTANCE {
	() => {
		0x19
	};
}
#[macro_export]
macro_rules! ABS_TILT_X {
	() => {
		0x1a
	};
}
#[macro_export]
macro_rules! ABS_TILT_Y {
	() => {
		0x1b
	};
}
#[macro_export]
macro_rules! ABS_TOOL_WIDTH {
	() => {
		0x1c
	};
}

#[macro_export]
macro_rules! ABS_VOLUME {
	() => {
		0x20
	};
}

#[macro_export]
macro_rules! ABS_MISC {
	() => {
		0x28
	};
}

#[macro_export]
macro_rules! ABS_RESERVED {
	() => {
		0x2e
	};
}

#[macro_export]
macro_rules! ABS_MT_SLOT {
	() => {
		0x2f
	};
}
#[macro_export]
macro_rules! ABS_MT_TOUCH_MAJOR {
	() => {
		0x30
	};
}
#[macro_export]
macro_rules! ABS_MT_TOUCH_MINOR {
	() => {
		0x31
	};
}
#[macro_export]
macro_rules! ABS_MT_WIDTH_MAJOR {
	() => {
		0x32
	};
}
#[macro_export]
macro_rules! ABS_MT_WIDTH_MINOR {
	() => {
		0x33
	};
}
#[macro_export]
macro_rules! ABS_MT_ORIENTATION {
	() => {
		0x34
	};
}
#[macro_export]
macro_rules! ABS_MT_POSITION_X {
	() => {
		0x35
	};
}
#[macro_export]
macro_rules! ABS_MT_POSITION_Y {
	() => {
		0x36
	};
}
#[macro_export]
macro_rules! ABS_MT_TOOL_TYPE {
	() => {
		0x37
	};
}
#[macro_export]
macro_rules! ABS_MT_BLOB_ID {
	() => {
		0x38
	};
}
#[macro_export]
macro_rules! ABS_MT_TRACKING_ID {
	() => {
		0x39
	};
}
#[macro_export]
macro_rules! ABS_MT_PRESSURE {
	() => {
		0x3a
	};
}
#[macro_export]
macro_rules! ABS_MT_DISTANCE {
	() => {
		0x3b
	};
}
#[macro_export]
macro_rules! ABS_MT_TOOL_X {
	() => {
		0x3c
	};
}
#[macro_export]
macro_rules! ABS_MT_TOOL_Y {
	() => {
		0x3d
	};
}

#[macro_export]
macro_rules! ABS_MAX {
	() => {
		0x3f
	};
}
#[macro_export]
macro_rules! ABS_CNT {
	() => {
		($crate::ABS_MAX!() + 1)
	};
}
