#[macro_export]
macro_rules! REP_DELAY {
	() => {
		0x00
	};
}
#[macro_export]
macro_rules! REP_PERIOD {
	() => {
		0x01
	};
}
#[macro_export]
macro_rules! REP_MAX {
	() => {
		0x01
	};
}
#[macro_export]
macro_rules! REP_CNT {
	() => {
		($crate::REP_MAX!() + 1)
	};
}
