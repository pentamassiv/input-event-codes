#[macro_export]
macro_rules! REL_X {
	() => {
		0x00
	};
}
#[macro_export]
macro_rules! REL_Y {
	() => {
		0x01
	};
}
#[macro_export]
macro_rules! REL_Z {
	() => {
		0x02
	};
}
#[macro_export]
macro_rules! REL_RX {
	() => {
		0x03
	};
}
#[macro_export]
macro_rules! REL_RY {
	() => {
		0x04
	};
}
#[macro_export]
macro_rules! REL_RZ {
	() => {
		0x05
	};
}
#[macro_export]
macro_rules! REL_HWHEEL {
	() => {
		0x06
	};
}
#[macro_export]
macro_rules! REL_DIAL {
	() => {
		0x07
	};
}
#[macro_export]
macro_rules! REL_WHEEL {
	() => {
		0x08
	};
}
#[macro_export]
macro_rules! REL_MISC {
	() => {
		0x09
	};
}
#[macro_export]
macro_rules! REL_RESERVED {
	() => {
		0x0a
	};
}
#[macro_export]
macro_rules! REL_WHEEL_HI_RES {
	() => {
		0x0b
	};
}
#[macro_export]
macro_rules! REL_HWHEEL_HI_RES {
	() => {
		0x0c
	};
}
#[macro_export]
macro_rules! REL_MAX {
	() => {
		0x0f
	};
}
#[macro_export]
macro_rules! REL_CNT {
	() => {
		($crate::REL_MAX!() + 1)
	};
}
