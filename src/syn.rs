#[macro_export]
macro_rules! SYN_REPORT {
	() => {
		0
	};
}
#[macro_export]
macro_rules! SYN_CONFIG {
	() => {
		1
	};
}
#[macro_export]
macro_rules! SYN_MT_REPORT {
	() => {
		2
	};
}
#[macro_export]
macro_rules! SYN_DROPPED {
	() => {
		3
	};
}
#[macro_export]
macro_rules! SYN_MAX {
	() => {
		0xf
	};
}
#[macro_export]
macro_rules! SYN_CNT {
	() => {
		($crate::SYN_MAX!() + 1)
	};
}
