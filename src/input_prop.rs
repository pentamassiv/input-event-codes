#[macro_export]
macro_rules! INPUT_PROP_POINTER {
	() => {
		0x00
	};
}
#[macro_export]
macro_rules! INPUT_PROP_DIRECT {
	() => {
		0x01
	};
}
#[macro_export]
macro_rules! INPUT_PROP_BUTTONPAD {
	() => {
		0x02
	};
}
#[macro_export]
macro_rules! INPUT_PROP_SEMI_MT {
	() => {
		0x03
	};
}
#[macro_export]
macro_rules! INPUT_PROP_TOPBUTTONPAD {
	() => {
		0x04
	};
}
#[macro_export]
macro_rules! INPUT_PROP_POINTING_STICK {
	() => {
		0x05
	};
}
#[macro_export]
macro_rules! INPUT_PROP_ACCELEROMETER {
	() => {
		0x06
	};
}

#[macro_export]
macro_rules! INPUT_PROP_MAX {
	() => {
		0x1f
	};
}
#[macro_export]
macro_rules! INPUT_PROP_CNT {
	() => {
		($crate::INPUT_PROP_MAX!() + 1)
	};
}
