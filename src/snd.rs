#[macro_export]
macro_rules! SND_CLICK {
	() => {
		0x00
	};
}
#[macro_export]
macro_rules! SND_BELL {
	() => {
		0x01
	};
}
#[macro_export]
macro_rules! SND_TONE {
	() => {
		0x02
	};
}
#[macro_export]
macro_rules! SND_MAX {
	() => {
		0x07
	};
}
#[macro_export]
macro_rules! SND_CNT {
	() => {
		($crate::SND_MAX!() + 1)
	};
}
